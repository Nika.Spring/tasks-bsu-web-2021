﻿using System.Text.RegularExpressions;

namespace BooksApp
{
    /*
     * Implementation for the task:
     * https://gitlab.com/AnzhelikaKravchuk/bsu-web-2021/-/blob/main/isbn-verification-readme.md
     */
    public class Isbn10Verifier
    {
        // Based on Regular Expressions Cookbook https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s13.html
        private const string Pattern = @"^(?=[0-9X]{10}$|(?=(?:[0-9]+[-]){3})[-0-9X]{13}$)[0-9]{1,5}[-]?[0-9]+[-]?[0-9]+[-]?[0-9X]$";

        public static bool IsValid(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
                return false;

            if (!Regex.IsMatch(isbn, Pattern))
                return false;

            isbn = isbn.Replace("-", string.Empty);

            var sum = 0;
            for (var i = 0; i < 10; i++)
            {
                var digit = isbn[i];
                if (digit >= '0' && digit <= '9')
                {
                    sum += (10 - i) * (digit - '0');
                }
                else if (i == 9 && (digit == 'X'))
                {
                    sum += 10;
                }
            }

            return sum % 11 == 0;
        }
    }
}
