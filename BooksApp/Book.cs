﻿using System;

namespace BooksApp
{
    /*
     * Implementation for the task:
     * https://gitlab.com/AnzhelikaKravchuk/bsu-web-2021/-/blob/main/book-comparable.md 
     * https://gitlab.com/AnzhelikaKravchuk/bsu-web-2021/-/blob/main/oop-class-book.md
     */
    public sealed class Book : IEquatable<Book>, IComparable<Book>, IComparable
    {
        private bool published;
        private DateTime datePublished;
        private int totalPages;
        public string Author { get; }
        public string Title { get; }
        public string Publisher { get; }
        public string ISBN { get; }
        public decimal Price { get; private set; }
        public string Currency { get; private set; }
        public int Pages
        {
            get { return totalPages; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Pages", "Pages should be positive number");
                }
                else
                {
                    totalPages = value;
                }
            }
        }

        public Book(string author, string title, string publisher)
        {
            Author = author;
            Title = title;
            Publisher = publisher;
        }

        public Book(string author, string title, string publisher, string isbn) : this(author, title, publisher)
        {
            ISBN = isbn;
        }

        public Book Publish(DateTime publishDateTime)
        {
            published = true;
            datePublished = publishDateTime;
            return this;
        }

        public string GetPublicationDate()
        {
            if (published)
                return datePublished.ToString();
            else
                return "NYP";
        }

        public Book SetPrice(decimal price, string currency)
        {
            Price = price;
            Currency = currency;
            return this;
        }

        public override bool Equals(object? obj)
        {
            if (!(obj is Book other))
            {
                return false;
            }
            else
            {
                if (string.IsNullOrEmpty(other.ISBN) || string.IsNullOrEmpty(ISBN))
                {
                    return base.Equals(obj);
                }
                return ISBN.Equals(other.ISBN);
            }
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(ISBN))
            {
                return base.GetHashCode();
            }
            return ISBN.GetHashCode();
        }

        public override string? ToString()
        {
            return Title + " by " + Author;
        }

        public bool Equals(Book? other)
        {
            if (other == null)
                return false;
            return ISBN.Equals(other.ISBN);
        }

        public int CompareTo(Book? other)
        {
            if (other == null)
                return -1;
            else
                return ISBN.CompareTo(other.ISBN);
        }

        public int CompareTo(object? obj)
        {
            if (!(obj is Book book))
            {
                return -1;
            }
            else
            {
                return ISBN.CompareTo(book.ISBN);
            }
        }
    }
}
