﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BooksApp
{
    /*
     * Implementation for the task:
     * https://gitlab.com/AnzhelikaKravchuk/bsu-web-2021/-/blob/main/book-service.md
     */
    public class BookListService
    {
        private FakeBookStorage bookStorage;

        public BookListService(FakeBookStorage bookStorage)
        {
            this.bookStorage = bookStorage;
        }

        public void Add(Book book)
        {
            if (bookStorage.Contains(book))
            {
                throw new ArgumentException("Book already stored");
            }
            bookStorage.Add(book);
        }

        public void Remove(Book book)
        {
            if (!bookStorage.Contains(book))
            {
                throw new ArgumentException("No such book");
            }
            bookStorage.Remove(book);
        }

        /*
         * Find by Title or Author
         */
        public IList<Book> Find(string searchTerm)
        {
            List<Book> result = new List<Book>();
            foreach (var book in bookStorage)
            {
                if (book.Author.Contains(searchTerm) || book.Title.Contains(searchTerm))
                {
                    result.Add(book);
                }
            }
            return result;
        }

        public IList<Book> GetBy(BookSort bookSort)
        {
            List<Book> books = bookStorage.ToList();
            switch (bookSort)
            {
                case BookSort.ByAuthor:
                    books.Sort(new BookAuthorComparer());
                    break;
                case BookSort.ByTitle:
                    books.Sort(new BookTitleComparer());
                    break;
            }
            return books;
        }

        public IList<Book> Load()
        {
            return bookStorage.ToList();
        }

        public void Save(IList<Book> books)
        {
            foreach (var book in books)
            {
                Add(book);
            }
        }

    }

    public enum BookSort
    {
        ByAuthor,
        ByTitle
    }

    class BookAuthorComparer : IComparer<Book>
    {
        public int Compare(Book? x, Book? y)
        {
            if (x is null || y is null)
                throw new ArgumentException("Invalid params");

            return string.Compare(x.Author, y.Author, StringComparison.Ordinal);
        }
    }
    class BookTitleComparer : IComparer<Book>
    {
        public int Compare(Book? x, Book? y)
        {
            if (x is null || y is null)
                throw new ArgumentException("Invalid params");

            return string.Compare(x.Title, y.Title, StringComparison.Ordinal);
        }
    }

}
