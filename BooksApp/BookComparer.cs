﻿using System;
using System.Collections.Generic;

namespace BooksApp
{
    /*
     * Implementation for the task:
     * https://gitlab.com/AnzhelikaKravchuk/bsu-web-2021/-/blob/main/book-comparators.md 
     */
    public class BookComparer : IComparer<Book>
    {
        public int Compare(Book? x, Book? y)
        {
            if (x is null || y is null)
                throw new ArgumentException("Invalid params");

            int authorCompare = string.Compare(x.Author, y.Author, StringComparison.Ordinal);
            if (authorCompare == 0)
            {
                int pagesCompare = x.Pages.CompareTo(y.Pages);
                if (pagesCompare == 0)
                {
                    return x.Price.CompareTo(y.Price);
                }
                else
                {
                    return pagesCompare;
                }
            }
            else
            {
                return authorCompare;
            }
        }
    }
}
