﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace BooksApp.Tests
{
    [TestClass()]
    public class BookListServiceTests
    {

        [TestMethod()]
        public void AddTest()
        {
            // Arrange
            FakeBookStorage bookStorage = new FakeBookStorage();
            BookListService bookListService = new BookListService(bookStorage);
            Book book = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");

            // Act
            bookListService.Add(book);

            // Assert
            Assert.IsTrue(bookStorage.Contains(book));
        }

        [TestMethod()]
        public void RemoveTest()
        {
            // Arrange
            FakeBookStorage bookStorage = new FakeBookStorage();
            BookListService bookListService = new BookListService(bookStorage);
            Book book = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");
            bookListService.Add(book);

            // Act
            bookListService.Remove(book);

            // Assert
            Assert.IsTrue(bookStorage.Count == 0);
        }

        [TestMethod()]
        public void FindTest()
        {
            // Arrange
            FakeBookStorage bookStorage = new FakeBookStorage();
            BookListService bookListService = new BookListService(bookStorage);
            Book book1 = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");
            bookListService.Add(book1);
            Book book2 = new Book("Tugberk Ugurlu", "Pro ASP.NET Web API: HTTP Web Services in ASP.NET", "Apress", "9781430247258");
            bookListService.Add(book2);
            Book book3 = new Book("Tomas Petricek and Jon Skeet", "Real - World Functional Programming with Examples in F# and C#", "Manning Publications", "9781933988924");
            bookListService.Add(book3);

            // Act
            IList<Book> books = bookListService.Find("Jon Skeet");

            // Assert
            Assert.IsTrue(books.Count == 2);
            Assert.IsTrue(books.Contains(book1));
            Assert.IsTrue(books.Contains(book3));
        }

        [TestMethod()]
        public void GetByTest()
        {
            // Arrange
            FakeBookStorage bookStorage = new FakeBookStorage();
            BookListService bookListService = new BookListService(bookStorage);
            Book book1 = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");
            bookListService.Add(book1);
            Book book2 = new Book("Tugberk Ugurlu", "Pro ASP.NET Web API: HTTP Web Services in ASP.NET", "Apress", "9781430247258");
            bookListService.Add(book2);
            Book book3 = new Book("Jon Skeet and Tomas Petricek", "Real - World Functional Programming with Examples in F# and C#", "Manning Publications", "9781933988924");
            bookListService.Add(book3);

            // Act
            IList<Book> books = bookListService.GetBy(BookSort.ByAuthor);

            // Assert
            CollectionAssert.AreEqual(new List<Book>() { book1, book3, book2 }, (System.Collections.ICollection)books);
        }

        [TestMethod()]
        public void LoadTest()
        {
            // Arrange
            FakeBookStorage bookStorage = new FakeBookStorage();
            BookListService bookListService = new BookListService(bookStorage);
            Book book1 = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");
            bookListService.Add(book1);
            Book book2 = new Book("Tugberk Ugurlu", "Pro ASP.NET Web API: HTTP Web Services in ASP.NET", "Apress", "9781430247258");
            bookListService.Add(book2);
            Book book3 = new Book("Jon Skeet and Tomas Petricek", "Real - World Functional Programming with Examples in F# and C#", "Manning Publications", "9781933988924");
            bookListService.Add(book3);

            // Act
            IList<Book> books = bookListService.Load();

            // Assert
            CollectionAssert.AreEquivalent(new List<Book>() { book1, book3, book2 }, (System.Collections.ICollection)books);
        }

        [TestMethod()]
        public void SaveTest()
        {
            // Arrange
            FakeBookStorage bookStorage = new FakeBookStorage();
            BookListService bookListService = new BookListService(bookStorage);
            Book book1 = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");
            Book book2 = new Book("Tugberk Ugurlu", "Pro ASP.NET Web API: HTTP Web Services in ASP.NET", "Apress", "9781430247258");
            Book book3 = new Book("Jon Skeet and Tomas Petricek", "Real - World Functional Programming with Examples in F# and C#", "Manning Publications", "9781933988924");

            // Act
            bookListService.Save(new List<Book>() { book1, book2, book3 });

            // Assert
            CollectionAssert.AreEquivalent(new List<Book>() { book1, book3, book2 }, (System.Collections.ICollection)bookListService.Load());
        }
    }
}