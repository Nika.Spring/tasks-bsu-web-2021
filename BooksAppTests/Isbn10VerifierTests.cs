﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace BooksApp.Tests
{
    [TestClass()]
    public class Isbn10VerifierTests
    {
        [TestMethod()]
        public void IsValidTest()
        {
            // Arrange
            var testIsbns = new Dictionary<string, bool>()
            {
                {"3-598-21508-8", true },
                {"3-598-21508-9", false },
                {"3-598-21507-X", true }
            };

            var actualTestIsbns = new Dictionary<string, bool>();
            // Act 
            foreach (var isbnTest in testIsbns)
            {
                actualTestIsbns.Add(isbnTest.Key, Isbn10Verifier.IsValid(isbnTest.Key));
            }
            
            // Assert
            CollectionAssert.AreEquivalent(actualTestIsbns, testIsbns);
        }
    }
}