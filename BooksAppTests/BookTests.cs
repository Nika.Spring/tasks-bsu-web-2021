﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace BooksApp.Tests
{
    [TestClass()]
    public class BookTests
    {
        [TestMethod()]
        public void BookPagesTest()
        {
            // Arrange
            Book book = new Book("Jon Skeet", "C# in Depth", "Manning Publications");

            // Act & Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(
                () =>
                {
                    book.Pages = -1;
                }
            );
        }

        [TestMethod()]
        public void BookPublishTest()
        {
            // Arrange
            Book book = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");

            // Act
            DateTime now = DateTime.Now;
            book.Publish(now);

            // Assert
            Assert.AreEqual(now.ToString(), book.GetPublicationDate());
        }

    }
}