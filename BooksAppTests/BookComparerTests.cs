﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;

namespace BooksApp.Tests
{
    [TestClass()]
    public class BookComparerTests
    {
        [TestMethod()]
        public void CompareTest()
        {
            // Arrange
            var region = new RegionInfo("US");
            Book book1 = new Book("Jon Skeet", "C# in Depth", "Manning Publications", "9781617294532");
            book1.Pages = 528;
            book1.SetPrice(25.19m, region.ISOCurrencySymbol);
            Book book2 = new Book("Jon Skeet", "Software Mistakes and Tradeoffs", "Manning Publications", "9781617299209");
            book2.Pages = 391;
            book2.SetPrice(30.23m, region.ISOCurrencySymbol);
            Book[] books = { book1, book2 };

            // Act
            Array.Sort(books, new BookComparer());

            // Assert
            CollectionAssert.AreEqual(new Book[] { book2, book1 }, books);
        }
    }
}
